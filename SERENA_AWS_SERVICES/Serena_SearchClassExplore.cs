﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SERENA_AWS_SERVICES
{
    public class Serena_SearchClassExplore
    {
        public string AvgRating { get; set; }
        public string TotalReviews { get; set; }
        public string guid { get; set; }
        public string Title { get; set; }
        public string CoverPhotoUrl { get; set; }
        public string Mentions { get; set; } 
    }
}
