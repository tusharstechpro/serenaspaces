﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SERENA_AWS_SERVICES
{
   public class Serena_UserProfile_Pro
    {
         
        public string UserEmailId { get; set; }
        public string FullName { get; set; }
        public string UserName { get; set; }
        public string UserWebsite { get; set; }
        public string UserLocation { get; set; }
        public string AllowContact { get; set; }
        public string ContactEmail { get; set; }
        public string ContactPhone { get; set; }
        public string IsPro { get; set; }


    }
}
