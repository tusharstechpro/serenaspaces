﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SERENA_AWS_SERVICES
{
    public class Serena_Topics_RDS
    {
        public int Id { get; set; }
        public string TopicName { get; set; }
        public string LastCreated { get; set; }
        public string LastUpdated { get; set; }
        public string LastLoginTime { get; set; } 
    }
}
