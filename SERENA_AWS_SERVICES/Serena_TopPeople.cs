﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SERENA_AWS_SERVICES
{
    public class Serena_TopPeople
    {
        
        public string UserEmailId { get; set; }
        public string UserName { get; set; }
        public string UserProfileImage { get; set; }
        public string AvgRating { get; set; }
        public string TotalReviews { get; set; }
        public string Topics { get; set; }

    }
}
