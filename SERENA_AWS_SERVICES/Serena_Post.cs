﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SERENA_AWS_SERVICES
{
    public class Serena_Post
    {
        
        public string Id { get; set; }
        public string UserEmailId { get; set; }
        public string Description { get; set; }
        public string ShareType { get; set; }
        public string Location { get; set; }
        public string Topics { get; set; }
        public string ImageUrl { get; set; }
        public string Tags{ get; set; }
        public string HashTags{ get; set; }
        public string LastCreated { get; set; }
        public string LastUpdated { get; set; }
        public string TimeOfPost { get; set; }

        public string PostText { get; set; }
        public string PostColor { get; set; }
        public string BackgroundColor { get; set; }

        public double Opacity { get; set; }
        public string PlaceAddress { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }
        public string PostType { get; set; }
        public string Guid { get; set; }
        public string Favourite { get; set; }
        public string FavCount { get; set; }


    }
}
 
 
 