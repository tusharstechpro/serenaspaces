using Amazon;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.DocumentModel;
using Amazon.DynamoDBv2.Model;
using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Core;
using Amazon.Runtime;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Net;
using System.Threading.Tasks;
// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.SystemTextJson.DefaultLambdaJsonSerializer))]

namespace SERENA_AWS_SERVICES
{
    public class Function
    {
        private readonly string connectionString;

        private readonly string _accessKey;
        private readonly string _secretKey;
        private readonly string _serviceUrl;
        public const string ID_QUERY_STRING_NAME = "Id";
        public const string Email_ID_QUERY_STRING_NAME = "UserEmailId";

        IDynamoDBContext DDBContext { get; set; }
        const string TABLENAME_ENVIRONMENT_VARIABLE_LOOKUP = "Serena_User";
        private const string table_serena_User = "Serena_User";
        private const string table_serena_UserProfile = "Serena_UserProfile";
        private const string table_serena_Post = "Serena_Post";
        private const string table_serena_AgeRange = "Serena_AgeRange";


        public Function()
        {


            connectionString = "SERVER=serena.ckqiggg12g3h.us-east-2.rds.amazonaws.com;" + "DATABASE=serena;" + "UID=admin;" + "PASSWORD=techpro30#;";

            _accessKey = Environment.GetEnvironmentVariable("AccessKey");
            _secretKey = Environment.GetEnvironmentVariable("SecretKey");
            _serviceUrl = Environment.GetEnvironmentVariable("ServiceURL");
            var tableName = System.Environment.GetEnvironmentVariable(TABLENAME_ENVIRONMENT_VARIABLE_LOOKUP);
            if (!string.IsNullOrEmpty(tableName))
            {
                AWSConfigsDynamoDB.Context.TypeMappings[typeof(Serena_User)] = new Amazon.Util.TypeMapping(typeof(Serena_User), tableName);
            }

            var config = new DynamoDBContextConfig { Conversion = DynamoDBEntryConversion.V2 };
            this.DDBContext = new DynamoDBContext(new AmazonDynamoDBClient(), config);
        }

        #region Serena_User 
        //Sign Up - Insert/update records in Serena_User table
        public async Task<APIGatewayProxyResponse> Save_Serena_User(Serena_User user, ILambdaContext context)
        {

            var dynamoDbClient = new AmazonDynamoDBClient(
            new BasicAWSCredentials(_accessKey, _secretKey),
            new AmazonDynamoDBConfig
            {
                ServiceURL = "https://dynamodb.us-east-2.amazonaws.com",
                RegionEndpoint = RegionEndpoint.USEast2
            });

            await dynamoDbClient.PutItemAsync(table_serena_User, new Dictionary<string, AttributeValue>
            {
                { "Id", new AttributeValue(user.Id.Trim()) },
                { "Password", new AttributeValue(user.Password) },
                { "FullName", new AttributeValue(user.FullName) },
                { "UserName", new AttributeValue(user.UserName) },
                { "Place", new AttributeValue(user.Place)},
                { "UserCategory", new AttributeValue(user.UserCategory)},
                { "UserLoginType", new AttributeValue(user.UserLoginType)},
                { "UserStatus", new AttributeValue(user.UserStatus)},
                { "LastCreated", new AttributeValue(user.LastCreated)},
                { "LastUpdated", new AttributeValue(user.LastUpdated)},
                { "LastLoginTime", new AttributeValue(user.LastLoginTime)},
             });
            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = "Record Saved Successfully!!!",
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } }
            };

            return response;
        }

        //get by email id retrieve single record from Serena_User table
        public async Task<APIGatewayProxyResponse> Get_Serena_UserById(APIGatewayProxyRequest request)
        {
            string userId = null;
            if (request.PathParameters != null && request.PathParameters.ContainsKey(ID_QUERY_STRING_NAME))
                userId = request.PathParameters[ID_QUERY_STRING_NAME];
            else if (request.QueryStringParameters != null && request.QueryStringParameters.ContainsKey(ID_QUERY_STRING_NAME))
                userId = request.QueryStringParameters[ID_QUERY_STRING_NAME];

            if (string.IsNullOrEmpty(userId))
            {
                return new APIGatewayProxyResponse
                {
                    StatusCode = (int)HttpStatusCode.BadRequest,
                    Body = $"Missing required parameter {ID_QUERY_STRING_NAME}"
                };
            }

            var regUSer = await DDBContext.LoadAsync<Serena_User>(userId);

            if (regUSer == null)
            {
                return new APIGatewayProxyResponse
                {
                    StatusCode = (int)HttpStatusCode.BadRequest,
                    Body = $"Invalid User"
                };
            }

            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = JsonConvert.SerializeObject(regUSer),
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } }
            };
            return response;
        }


        //get all registered users from dynamodb
        public async Task<APIGatewayProxyResponse> Get_AllSerena_Users()
        {
            var amazonDynamoDBConfig = new Amazon.DynamoDBv2.AmazonDynamoDBConfig { RegionEndpoint = RegionEndpoint.USEast2 };
            var client = new Amazon.DynamoDBv2.AmazonDynamoDBClient(amazonDynamoDBConfig);
            var myTable = Table.LoadTable(client, "Serena_User");
            var scanFilter = new ScanFilter();
            scanFilter.AddCondition("Id", ScanOperator.IsNotNull); //Whatever your filter is
            var search = myTable.Scan(scanFilter);
            var docList = await search.GetNextSetAsync();

            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = JsonConvert.SerializeObject(docList),
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } }
            };

            return response;
            //return results;
        }

        #endregion

        #region Serena_UserProfile

        //Sign Up - Insert/update records in Serena_UserProfile dynamodb table
        public async Task<APIGatewayProxyResponse> Save_Serena_UserProfile(Serena_UserProfile userProfile, ILambdaContext context)
        {

            var dynamoDbClient = new AmazonDynamoDBClient(
            new BasicAWSCredentials(_accessKey, _secretKey),
            new AmazonDynamoDBConfig
            {
                ServiceURL = "https://dynamodb.us-east-2.amazonaws.com",
                RegionEndpoint = RegionEndpoint.USEast2
            });

            await dynamoDbClient.PutItemAsync(table_serena_UserProfile, new Dictionary<string, AttributeValue>
            {
                { "Id", new AttributeValue(userProfile.Id) },
                { "UserEmailId", new AttributeValue(userProfile.UserEmailId) },
                { "UserFollowerJson", new AttributeValue(userProfile.UserFollowerJson) },
                { "UserFollowingJson", new AttributeValue(userProfile.UserFollowingJson) },
                { "UserPostJson", new AttributeValue(userProfile.UserPostJson)},
                { "UserProfileImage", new AttributeValue(userProfile.UserProfileImage)},
                { "UserAccountType", new AttributeValue(userProfile.UserAccountType)},
                { "UserBio", new AttributeValue(userProfile.UserBio)},
                { "UserWebsite", new AttributeValue(userProfile.UserWebsite)},
                { "UserLocation", new AttributeValue(userProfile.UserLocation)},
                { "UserAgeRange", new AttributeValue(userProfile.UserAgeRange)},
             });
            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = "Record Saved Successfully!!!",
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } }
            };

            return response;
        }

        public async Task<APIGatewayProxyResponse> Get_All_UsersProfile()
        {
            var amazonDynamoDBConfig = new Amazon.DynamoDBv2.AmazonDynamoDBConfig { RegionEndpoint = RegionEndpoint.USEast2 };
            var client = new Amazon.DynamoDBv2.AmazonDynamoDBClient(amazonDynamoDBConfig);
            var myTable = Table.LoadTable(client, "Serena_UserProfile");
            var scanFilter = new ScanFilter();
            scanFilter.AddCondition("Id", ScanOperator.IsNotNull); //Whatever your filter is
            var search = myTable.Scan(scanFilter);
            var docList = await search.GetNextSetAsync();

            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = JsonConvert.SerializeObject(docList),
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } }
            };

            return response;
        }

        //get by profile id retrieve single record from Serena_UserProfile table
        public async Task<APIGatewayProxyResponse> Get_Serena_UserProfileById(APIGatewayProxyRequest request)
        {
            string userId = null;
            if (request.PathParameters != null && request.PathParameters.ContainsKey(ID_QUERY_STRING_NAME))
                userId = request.PathParameters[ID_QUERY_STRING_NAME];
            else if (request.QueryStringParameters != null && request.QueryStringParameters.ContainsKey(ID_QUERY_STRING_NAME))
                userId = request.QueryStringParameters[ID_QUERY_STRING_NAME];

            if (string.IsNullOrEmpty(userId))
            {
                return new APIGatewayProxyResponse
                {
                    StatusCode = (int)HttpStatusCode.BadRequest,
                    Body = $"Missing required parameter {ID_QUERY_STRING_NAME}"
                };
            }

            var regUSer = await DDBContext.LoadAsync<Serena_UserProfile>(userId);

            if (regUSer == null)
            {
                return new APIGatewayProxyResponse
                {
                    StatusCode = (int)HttpStatusCode.BadRequest,
                    Body = $"Invalid User"
                };
            }

            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = JsonConvert.SerializeObject(regUSer),
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } }
            };
            return response;
        }

        #endregion

        #region Serena_AgeLimit
        public async Task<APIGatewayProxyResponse> Save_Serena_AgeRange(Serena_AgeRange AgeRange, ILambdaContext context)
        {

            var dynamoDbClient = new AmazonDynamoDBClient(
            new BasicAWSCredentials(_accessKey, _secretKey),
            new AmazonDynamoDBConfig
            {
                ServiceURL = "https://dynamodb.us-east-2.amazonaws.com",
                RegionEndpoint = RegionEndpoint.USEast2
            });

            await dynamoDbClient.PutItemAsync(table_serena_AgeRange, new Dictionary<string, AttributeValue>
            {
                { "Id", new AttributeValue(AgeRange.ID) },
                { "AgeRange", new AttributeValue(AgeRange.AgeRange) }
             });
            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = "Record Saved Successfully!!!",
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } }
            };

            return response;
        }


        public async Task<APIGatewayProxyResponse> Get_AllSerena_AgeRange()
        {
            var amazonDynamoDBConfig = new Amazon.DynamoDBv2.AmazonDynamoDBConfig { RegionEndpoint = RegionEndpoint.USEast2 };
            var client = new Amazon.DynamoDBv2.AmazonDynamoDBClient(amazonDynamoDBConfig);
            var myTable = Table.LoadTable(client, "Serena_AgeRange");
            var scanFilter = new ScanFilter();
            scanFilter.AddCondition("Id", ScanOperator.IsNotNull); //Whatever your filter is
            var search = myTable.Scan(scanFilter);
            var docList = await search.GetNextSetAsync();

            var response = new APIGatewayProxyResponse
            {
                Headers = new Dictionary<string, string> { { "Content-Type", "application/text" } },
                StatusCode = (int)HttpStatusCode.OK,
                Body = JsonConvert.SerializeObject(docList)

            };

            return response;

        }

        #endregion

        #region Serena_Post
        public async Task<APIGatewayProxyResponse> Save_Serena_Post(Serena_Post userPost, ILambdaContext context)
        {

            var dynamoDbClient = new AmazonDynamoDBClient(
            new BasicAWSCredentials(_accessKey, _secretKey),
            new AmazonDynamoDBConfig
            {
                ServiceURL = "https://dynamodb.us-east-2.amazonaws.com",
                RegionEndpoint = RegionEndpoint.USEast2
            });

            await dynamoDbClient.PutItemAsync(table_serena_Post, new Dictionary<string, AttributeValue>
            {
                { "Id", new AttributeValue(userPost.Id) },
                { "UserEmailId", new AttributeValue(userPost.UserEmailId) },
                { "Description", new AttributeValue(userPost.Description) },
                { "ShareType", new AttributeValue(userPost.ShareType) },
                { "Location", new AttributeValue(userPost.Location)},
                { "Topics", new AttributeValue(userPost.Topics)},
                { "ImageUrl", new AttributeValue(userPost.ImageUrl)},
                { "Tags", new AttributeValue(userPost.Tags)},
                { "HashTags", new AttributeValue(userPost.HashTags)},
                { "LastCreated", new AttributeValue(userPost.LastCreated)},
                { "LastUpdated", new AttributeValue(userPost.LastUpdated)},
                { "TimeOfPost", new AttributeValue(userPost.TimeOfPost)},
                { "PostText", new AttributeValue(userPost.PostText)},
                { "PostColor", new AttributeValue(userPost.PostColor)},
                { "BackgroundColor", new AttributeValue(userPost.BackgroundColor)}


             });
            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = "Record Saved Successfully!!!",
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } }
            };

            return response;
        }

        public async Task<APIGatewayProxyResponse> Get_All_Posts()
        {
            var amazonDynamoDBConfig = new Amazon.DynamoDBv2.AmazonDynamoDBConfig { RegionEndpoint = RegionEndpoint.USEast2 };
            var client = new Amazon.DynamoDBv2.AmazonDynamoDBClient(amazonDynamoDBConfig);
            var myTable = Table.LoadTable(client, "Serena_Post");
            var scanFilter = new ScanFilter();
            scanFilter.AddCondition("Id", ScanOperator.IsNotNull); //Whatever your filter is
            var search = myTable.Scan(scanFilter);
            var docList = await search.GetNextSetAsync();

            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = JsonConvert.SerializeObject(docList),
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } }
            };

            return response;
        }

        //get by post id retrieve single record from Serena_Post table
        public async Task<APIGatewayProxyResponse> Get_Serena_PostById(APIGatewayProxyRequest request)
        {
            string userId = null;
            if (request.PathParameters != null && request.PathParameters.ContainsKey(ID_QUERY_STRING_NAME))
                userId = request.PathParameters[ID_QUERY_STRING_NAME];
            else if (request.QueryStringParameters != null && request.QueryStringParameters.ContainsKey(ID_QUERY_STRING_NAME))
                userId = request.QueryStringParameters[ID_QUERY_STRING_NAME];

            if (string.IsNullOrEmpty(userId))
            {
                return new APIGatewayProxyResponse
                {
                    StatusCode = (int)HttpStatusCode.BadRequest,
                    Body = $"Missing required parameter {ID_QUERY_STRING_NAME}"
                };
            }

            var getPost = await DDBContext.LoadAsync<Serena_Post>(userId);

            if (getPost == null)
            {
                return new APIGatewayProxyResponse
                {
                    StatusCode = (int)HttpStatusCode.BadRequest,
                    Body = $"Invalid User"
                };
            }

            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = JsonConvert.SerializeObject(getPost),
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } }
            };
            return response;
        }


        public async Task<APIGatewayProxyResponse> Get_Serena_PostByEmailId(APIGatewayProxyRequest request)
        {
            var amazonDynamoDBConfig = new Amazon.DynamoDBv2.AmazonDynamoDBConfig { RegionEndpoint = RegionEndpoint.USEast2 };
            var client = new Amazon.DynamoDBv2.AmazonDynamoDBClient(amazonDynamoDBConfig);
            var myTable = Table.LoadTable(client, "Serena_Post");
            var scanFilter = new ScanFilter();
            scanFilter.AddCondition("Id", ScanOperator.Equal, "10"); //Whatever your filter is
            var search = myTable.Scan(scanFilter);
            var docList = await search.GetNextSetAsync();

            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = JsonConvert.SerializeObject(docList),
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } }
            };

            return response;
        }

        public APIGatewayProxyResponse Get_Testdetails(APIGatewayProxyRequest request)
        {
            MySqlConnection connection = new MySqlConnection(connectionString);
            connection.Open();

            string testval = "test" + DateTime.Now.Minute.ToString();

            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = connection;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "insert into test values ('" + testval + "')";
            cmd.ExecuteNonQuery();
            connection.Close();

            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = "Record Saved Successfully!!!",
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } }
            };

            return response;
        }


        #endregion

        #region get perticulare serena user profile 

        public APIGatewayProxyResponse Get_Serena_UserProfile_ByID_RDS(Serena_UserProfile_RDS srd)
        {
            var lstsuser = new List<Serena_UserProfile_RDS>();

            var connection = new MySqlConnection(connectionString);
            var cmd = new MySqlCommand();
            cmd.Connection = connection;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT * from Serena_UserProfile where UserEmailId = '" + srd.UserEmailId + "'";
            connection.Open();
            var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                var sUser = new Serena_UserProfile_RDS();

                sUser.UserEmailId = reader[0].ToString();
                sUser.UserPost = reader[1].ToString();
                sUser.UserProfileImage = reader[2].ToString();
                sUser.UserAccountType = reader[3].ToString();
                sUser.UserBio = reader[4].ToString();
                sUser.UserWebsite = reader[5].ToString();
                sUser.UserLocation = reader[6].ToString();
                sUser.UserAgeRange = reader[7].ToString();
                sUser.UserName = reader[8].ToString();
                sUser.LastUpdated = reader[9].ToString();

                
                lstsuser.Add(sUser);
            }
            connection.Close();
            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = JsonConvert.SerializeObject(lstsuser, Formatting.Indented),
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } },
            };

            return response;
        }

        #endregion

        #region RDS-Serena_user

        public APIGatewayProxyResponse Save_SerenaUser_RDS(Serena_User_RDS user)
        {
            var connection = new MySqlConnection(connectionString);
            connection.Open();

            var cmd = new MySqlCommand();
            cmd.Connection = connection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "Sp_Insertinto_SerenaUsers";
            cmd.Parameters.AddWithValue("_EmailId", user.EMAIL);
            cmd.Parameters.AddWithValue("_Password", user.Password);
            cmd.Parameters.AddWithValue("_FullName", user.FullName);
            cmd.Parameters.AddWithValue("_UserName", user.UserName);
            cmd.Parameters.AddWithValue("_Place", user.Place);
            cmd.Parameters.AddWithValue("_UserCategory", user.UserCategory);
            cmd.Parameters.AddWithValue("_UserLoginType", user.UserLoginType);
            cmd.ExecuteNonQuery();
            connection.Close();

            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = "Record Saved Successfully!!!",
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } }
            };

            return response;

        }

        public APIGatewayProxyResponse Get_AllSerena_Users_RDS()
        {
            var lstsuser = new List<Serena_User_RDS>();

            var connection = new MySqlConnection(connectionString);
            var cmd = new MySqlCommand();
            cmd.Connection = connection;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT EmailId,FullName,UserName, Place,UserCategory,UserLoginType,LastCreated,LastUpdated,LastLoginTime, Password from Serena_User";
            connection.Open();
            var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                var sUser = new Serena_User_RDS();

                sUser.EMAIL = reader[0].ToString();
                sUser.FullName = reader[1].ToString();
                sUser.UserName = reader[2].ToString();
                sUser.Place = reader[3].ToString();
                sUser.UserCategory = reader[4].ToString();
                sUser.UserLoginType = reader[5].ToString();
                sUser.LastCreated = Convert.ToDateTime(reader[6]);
                sUser.LastUpdated = Convert.ToDateTime(reader[7]);
                sUser.LastLoginTime = Convert.ToDateTime(reader[8]);
                sUser.Password = reader[9].ToString();

                lstsuser.Add(sUser);
            }
            connection.Close();
            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = JsonConvert.SerializeObject(lstsuser, Formatting.Indented),
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } },
            };

            return response;
        }

        public APIGatewayProxyResponse Get_Serena_UsersByEmail_RDS(string EmailId)
        {
            var sUser = new Serena_User_RDS();

            var connection = new MySqlConnection(connectionString);
            var cmd = new MySqlCommand();
            cmd.Connection = connection;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT * from Serena_User where EmailId='" + EmailId + "'";
            connection.Open();
            var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                sUser.EMAIL = reader[0].ToString();
                sUser.FullName = reader[1].ToString();
                sUser.UserName = reader[2].ToString();
                sUser.Place = reader[3].ToString();
                sUser.UserCategory = reader[4].ToString();
                sUser.UserLoginType = reader[5].ToString();
                sUser.UserStatus = reader[6].ToString();
                sUser.LastCreated = Convert.ToDateTime(reader[7]);
                sUser.LastUpdated = Convert.ToDateTime(reader[8]);
                sUser.LastLoginTime = Convert.ToDateTime(reader[9]);
            }
            connection.Close();
            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = JsonConvert.SerializeObject(sUser),
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } }
            };

            return response;
        }

        #endregion

        #region RDS-Serena_AgeRange
        public APIGatewayProxyResponse Save_Serena_AgeRange_RDS(Serena_AgeRange_RDS ageRange)
        {

            var connection = new MySqlConnection(connectionString);
            connection.Open();

            var cmd = new MySqlCommand();
            cmd.Connection = connection;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "insert into Serena_AgeRange(AgeRange) values ('" + ageRange.AgeRange + "')";
            cmd.ExecuteNonQuery();
            connection.Close();

            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = "Record Saved Successfully!!!",
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } }
            };

            return response;
        }


        public APIGatewayProxyResponse Get_AllSerena_AgeRange_RDS()
        {
            var lstAgeRange = new List<Serena_AgeRange_RDS>();

            var connection = new MySqlConnection(connectionString);
            var cmd = new MySqlCommand();
            cmd.Connection = connection;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT * from Serena_AgeRange";
            connection.Open();
            var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                var sAgeRange = new Serena_AgeRange_RDS();

                sAgeRange.Id = Convert.ToInt32(reader[0]);
                sAgeRange.AgeRange = reader[1].ToString();

                lstAgeRange.Add(sAgeRange);
            }
            connection.Close();
            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = JsonConvert.SerializeObject(lstAgeRange, Formatting.Indented),
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } },
            };

            return response;

        }

        #endregion

        #region UserProfile


        public APIGatewayProxyResponse Update_Serena_UP_Tran_RDS(Serena_UprofileTran_RDS upt)
        {

            var connection = new MySqlConnection(connectionString);
            connection.Open();
            var cmd = new MySqlCommand();
            cmd.Connection = connection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "Sp_Update_UProfile_Tran";
            cmd.Parameters.AddWithValue("_Guid", upt.Guid);
            cmd.Parameters.AddWithValue("_Status", upt.Status);
            cmd.ExecuteNonQuery();
            connection.Close();
            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = "Record Updated Successfully!!!",
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } }
            };

            return response;
        }


        public APIGatewayProxyResponse Insert_Serena_UP_Tran_RDS(Serena_UprofileTran_RDS upt)
        {

            var connection = new MySqlConnection(connectionString);
            connection.Open();
            var cmd = new MySqlCommand();
            cmd.Connection = connection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "Sp_Insertinto_UProfile_Tran";
            cmd.Parameters.AddWithValue("_Guid", upt.Guid);
            cmd.Parameters.AddWithValue("_UserEmailId", upt.UserEmailId);
            cmd.Parameters.AddWithValue("_RequestTo", upt.RequestTo);
            cmd.Parameters.AddWithValue("_Status", upt.Status);
            cmd.ExecuteNonQuery();
            connection.Close();
            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = "Record Updated Successfully!!!",
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } }
            };

            return response;
        }

        #endregion

        #region UserProfile RDS

        public APIGatewayProxyResponse Insert_Serena_USerProfile_RDS(Serena_UserProfile_RDS upt)
        {

            var connection = new MySqlConnection(connectionString);
            connection.Open();
            var cmd = new MySqlCommand();
            cmd.Connection = connection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "Sp_InsertInto_UserProfile";
            cmd.Parameters.AddWithValue("_UserEmailId", upt.UserEmailId);
            cmd.Parameters.AddWithValue("_UserProfileImage", upt.UserProfileImage);
            cmd.Parameters.AddWithValue("_UserAccountType", upt.UserAccountType);
            cmd.Parameters.AddWithValue("_UserName", upt.UserName);

            cmd.ExecuteNonQuery();
            connection.Close();
            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = "Record Updated Successfully!!!",
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } }
            };

            return response;
        }

        public APIGatewayProxyResponse Update_Serena_USerProfile_RDS(Serena_UserProfile_RDS upt)
        {

            var connection = new MySqlConnection(connectionString);
            connection.Open();
            var cmd = new MySqlCommand();
            cmd.Connection = connection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "Sp_Update_UserProfile";
            cmd.Parameters.AddWithValue("_UserEmailId", upt.UserEmailId);
            cmd.Parameters.AddWithValue("_UserProfileImage", upt.UserProfileImage);
            cmd.Parameters.AddWithValue("_UserAccountType", upt.UserAccountType);
            cmd.Parameters.AddWithValue("_UserBio", upt.UserBio);
            cmd.Parameters.AddWithValue("_UserWebsite", upt.UserWebsite);
            cmd.Parameters.AddWithValue("_UserLocation", upt.UserLocation);
            cmd.Parameters.AddWithValue("_UserAgeRange", upt.UserAgeRange);
            cmd.Parameters.AddWithValue("_UserName", upt.UserName);

            cmd.ExecuteNonQuery();
            connection.Close();
            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = "Record Updated Successfully!!!",
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } }
            };

            return response;
        }


        public APIGatewayProxyResponse Get_Serena_UsersList_ProfilePage(Serena_UserProfile_RDS upt)
        {
            var lstsUser = new List<Serena_UserProfile_RDS>();

            var connection = new MySqlConnection(connectionString);
            var cmd = new MySqlCommand();
            cmd.Connection = connection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "Sp_Get_UsersList_ProfilePage";
            cmd.Parameters.AddWithValue("_UserEmailId", upt.UserEmailId);
            connection.Open();
            var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                var sUser = new Serena_UserProfile_RDS();
                sUser.UserEmailId = reader[0].ToString();
                sUser.UserName = reader[1].ToString();
                sUser.UserProfileImage = reader[2].ToString();
                sUser.TrequestStatus = reader[3].ToString();
                sUser.Guid = reader[4].ToString();
                sUser.UserAccountType = reader[5].ToString();
                sUser.UserBio = reader[6].ToString();
                sUser.UserWebsite = reader[7].ToString();
                sUser.UserLocation = reader[8].ToString();
                sUser.UserAgeRange = reader[9].ToString();
                sUser.LastUpdated = reader[10].ToString();

                sUser.IsPro = reader[11].ToString();
                sUser.AllowContact = reader[12].ToString();
                sUser.ContactEmail = reader[13].ToString();
                sUser.ContactPhone = reader[14].ToString();
                lstsUser.Add(sUser);
            }

            connection.Close();
            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = JsonConvert.SerializeObject(lstsUser, Formatting.Indented),
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } },
            };

            return response;
        }


        public APIGatewayProxyResponse Get_Serena_UsersList_ProfilePage_2(APIGatewayProxyRequest request)
        {
            string userId = null;
            if (request.PathParameters != null && request.PathParameters.ContainsKey(ID_QUERY_STRING_NAME))
                userId = request.PathParameters[ID_QUERY_STRING_NAME];
            else if (request.QueryStringParameters != null && request.QueryStringParameters.ContainsKey(ID_QUERY_STRING_NAME))
                userId = request.QueryStringParameters[ID_QUERY_STRING_NAME];

            if (string.IsNullOrEmpty(userId))
            {
                return new APIGatewayProxyResponse
                {
                    StatusCode = (int)HttpStatusCode.BadRequest,
                    Body = $"Missing required parameter {ID_QUERY_STRING_NAME}"
                };
            }


            var lstsUser = new List<Serena_UserProfile_RDS>();

            var connection = new MySqlConnection(connectionString);
            var cmd = new MySqlCommand();
            cmd.Connection = connection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "Sp_Get_UsersList_ProfilePage";
            cmd.Parameters.AddWithValue("_UserEmailId", userId);
            connection.Open();
            var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                var sUser = new Serena_UserProfile_RDS();
                sUser.UserEmailId = reader[0].ToString();
                sUser.UserName = reader[1].ToString();
                sUser.UserProfileImage = reader[2].ToString();
                sUser.TrequestStatus = reader[3].ToString();
                lstsUser.Add(sUser);
            }

            connection.Close();
            //var regUSer = await DDBContext.LoadAsync<Serena_User>(userId);

            if (lstsUser == null)
            {
                return new APIGatewayProxyResponse
                {
                    StatusCode = (int)HttpStatusCode.BadRequest,
                    Body = $"Invalid User"
                };
            }

            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = JsonConvert.SerializeObject(lstsUser),
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } }
            };
            return response;
        }


        #endregion

        #region POST rds

        public APIGatewayProxyResponse Get_AllSerena_Post_RDS(Serena_UserProfile_RDS upt)
        {
            var lstsuser = new List<Serena_Post>();

            var connection = new MySqlConnection(connectionString);
            var cmd = new MySqlCommand();
            cmd.Connection = connection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "Sp_GetAllPosts";
            cmd.Parameters.AddWithValue("_UserEmailId", upt.UserEmailId);
            connection.Open();
            var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                var sUser = new Serena_Post();
                sUser.Id = reader[0].ToString();
                sUser.UserEmailId = reader[1].ToString();
                sUser.Description = reader[2].ToString();
                sUser.ShareType = reader[3].ToString();
                sUser.Location = reader[4].ToString();
                sUser.Topics = reader[5].ToString();
                sUser.ImageUrl = reader[6].ToString();
                sUser.Tags = reader[7].ToString();
                sUser.HashTags = reader[8].ToString();
                sUser.LastCreated = reader[9].ToString();
                sUser.LastUpdated = reader[10].ToString();
                sUser.PostText = reader[11].ToString();
                sUser.PostColor = reader[12].ToString();
                sUser.BackgroundColor = reader[13].ToString();
                sUser.Opacity = Convert.ToDouble(reader[14]);
                sUser.PlaceAddress = reader[15].ToString();
                sUser.Longitude = reader[16].ToString();
                sUser.Latitude = reader[17].ToString();
                sUser.PostType = reader[18].ToString();
                sUser.Guid = reader[19].ToString();
                sUser.Favourite = reader[20].ToString();
                sUser.FavCount = reader[21].ToString();

                lstsuser.Add(sUser);
            }
            connection.Close();
            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = JsonConvert.SerializeObject(lstsuser, Formatting.Indented),
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } },
            };

            return response;
        }

        public APIGatewayProxyResponse Save_SerenaPost_RDS(Serena_Post sp)
        {
            var connection = new MySqlConnection(connectionString);
            connection.Open();

            var cmd = new MySqlCommand();
            cmd.Connection = connection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "Sp_Save_Serena_Post";
            cmd.Parameters.AddWithValue("_Id", "0");
            cmd.Parameters.AddWithValue("_UserEmailId", sp.UserEmailId);
            cmd.Parameters.AddWithValue("_Description", sp.Description);
            cmd.Parameters.AddWithValue("_ShareType", sp.ShareType);
            cmd.Parameters.AddWithValue("_Location", sp.Location);
            cmd.Parameters.AddWithValue("_Topics", sp.Topics);
            cmd.Parameters.AddWithValue("_ImageUrl", sp.ImageUrl);
            cmd.Parameters.AddWithValue("_Tags", sp.Tags);
            cmd.Parameters.AddWithValue("_HashTags", sp.HashTags);
            cmd.Parameters.AddWithValue("_PostText", sp.PostText);
            cmd.Parameters.AddWithValue("_PostColor", sp.PostColor);
            cmd.Parameters.AddWithValue("_BackgroundColor", sp.BackgroundColor);
            cmd.Parameters.AddWithValue("_PlaceAddress", sp.PlaceAddress);
            cmd.Parameters.AddWithValue("_Longitude", sp.Longitude);
            cmd.Parameters.AddWithValue("_Latitude", sp.Latitude);
            cmd.Parameters.AddWithValue("_PostType", sp.PostType);
            cmd.Parameters.AddWithValue("_Opacity", sp.Opacity);
            cmd.Parameters.AddWithValue("_Guid", sp.Guid);

            cmd.ExecuteNonQuery();
            connection.Close();

            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = "Record Saved Successfully!!!",
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } }
            };

            return response;

        }

        public APIGatewayProxyResponse Serena_FavPosts_RDS(Serena_Post sp)
        {
            var connection = new MySqlConnection(connectionString);
            connection.Open();

            var cmd = new MySqlCommand();
            cmd.Connection = connection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "Sp_InsertInto_Serena_FavPosts";
            cmd.Parameters.AddWithValue("_UserEmailId", sp.UserEmailId);
            cmd.Parameters.AddWithValue("_Guid", sp.Guid);
            cmd.Parameters.AddWithValue("_FavFlag", sp.Favourite);
            cmd.ExecuteNonQuery();
            connection.Close();

            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = "Record Saved Successfully!!!",
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } }
            };

            return response;

        }


        #endregion

        #region topics
        public APIGatewayProxyResponse Get_AllSerena_Topics_RDS()
        {
            var lstAgeRange = new List<Serena_Topics_RDS>();

            var connection = new MySqlConnection(connectionString);
            var cmd = new MySqlCommand();
            cmd.Connection = connection;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT * from Serena_Topics";
            connection.Open();
            var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                var topics = new Serena_Topics_RDS();

                topics.Id = Convert.ToInt32(reader[0]);
                topics.TopicName = reader[1].ToString();
                topics.LastCreated = reader[2].ToString();
                topics.LastUpdated = reader[3].ToString();


                lstAgeRange.Add(topics);
            }
            connection.Close();
            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = JsonConvert.SerializeObject(lstAgeRange, Formatting.Indented),
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } },
            };

            return response;

        }
        #endregion

        #region TopHashTags
        public APIGatewayProxyResponse Get_GetTopHashTags_Count_RDS()
        {
            var lstAgeRange = new List<Serena_TopHashTags>();

            var connection = new MySqlConnection(connectionString);
            var cmd = new MySqlCommand();
            cmd.Connection = connection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "Sp_GetTopHashTags";
            connection.Open();
            var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                var TopHashCnt = new Serena_TopHashTags();

                TopHashCnt.Count = reader[0].ToString();
                TopHashCnt.Hashtags = reader[1].ToString();
                TopHashCnt.Img1 = reader[2].ToString();
                TopHashCnt.Img2 = reader[3].ToString();
                TopHashCnt.Img3 = reader[4].ToString();
                lstAgeRange.Add(TopHashCnt);
            }
            connection.Close();
            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = JsonConvert.SerializeObject(lstAgeRange, Formatting.Indented),
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } },
            };

            return response;

        }

        public APIGatewayProxyResponse Get_GetTopHashTags2_Count_RDS()
        {
            var lstAgeRange = new List<Serena_TopHashTags2>();

            var connection = new MySqlConnection(connectionString);
            var cmd = new MySqlCommand();
            cmd.Connection = connection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "Sp_GetTopHashTags_2";
            connection.Open();
            var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                var TopHashCnt2 = new Serena_TopHashTags2();

                TopHashCnt2.Count = reader[0].ToString();
                TopHashCnt2.Hashtags = reader[1].ToString();
                TopHashCnt2.ImageUrl1 = reader[2].ToString();
                TopHashCnt2.PostText1 = reader[3].ToString();
                TopHashCnt2.PostColor1 = reader[4].ToString();
                TopHashCnt2.BackgroundColor1 = reader[5].ToString();
                TopHashCnt2.PlaceAddress1 = reader[6].ToString();
                TopHashCnt2.Longitude1 = reader[7].ToString();
                TopHashCnt2.Latitude1 = reader[8].ToString();
                TopHashCnt2.PostType1 = reader[9].ToString();
                TopHashCnt2.ImageUrl2 = reader[10].ToString();
                TopHashCnt2.PostText2 = reader[11].ToString();
                TopHashCnt2.PostColor2 = reader[12].ToString();
                TopHashCnt2.BackgroundColor2 = reader[13].ToString();
                TopHashCnt2.PlaceAddress2 = reader[14].ToString();
                TopHashCnt2.Longitude2 = reader[15].ToString();
                TopHashCnt2.Latitude2 = reader[16].ToString();
                TopHashCnt2.PostType2 = reader[17].ToString();
                TopHashCnt2.ImageUrl3 = reader[18].ToString();
                TopHashCnt2.PostText3 = reader[19].ToString();
                TopHashCnt2.PostColor3 = reader[20].ToString();
                TopHashCnt2.BackgroundColor3 = reader[21].ToString();
                TopHashCnt2.PlaceAddress3 = reader[22].ToString();
                TopHashCnt2.Longitude3 = reader[23].ToString();
                TopHashCnt2.Latitude3 = reader[24].ToString();
                TopHashCnt2.PostType3 = reader[25].ToString();
                TopHashCnt2.ImageUrl4 = reader[26].ToString();
                TopHashCnt2.PostText4 = reader[27].ToString();
                TopHashCnt2.PostColor4 = reader[28].ToString();
                TopHashCnt2.BackgroundColor4 = reader[29].ToString();
                TopHashCnt2.PlaceAddress4 = reader[30].ToString();
                TopHashCnt2.Longitude4 = reader[31].ToString();
                TopHashCnt2.Latitude4 = reader[32].ToString();
                TopHashCnt2.PostType4 = reader[33].ToString();
                TopHashCnt2.ImageUrl5 = reader[34].ToString();
                TopHashCnt2.PostText5 = reader[35].ToString();
                TopHashCnt2.PostColor5 = reader[36].ToString();
                TopHashCnt2.BackgroundColor5 = reader[37].ToString();
                TopHashCnt2.PlaceAddress5 = reader[38].ToString();
                TopHashCnt2.Longitude5 = reader[39].ToString();
                TopHashCnt2.Latitude5 = reader[40].ToString();
                TopHashCnt2.PostType5 = reader[41].ToString();


                lstAgeRange.Add(TopHashCnt2);
            }
            connection.Close();
            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = JsonConvert.SerializeObject(lstAgeRange, Formatting.Indented),
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } },
            };

            return response;

        }

        public APIGatewayProxyResponse SearchHashTags_RDS(Serena_TopHashTags sth)
        {
            var lstAgeRange = new List<Serena_TopHashTags>();

            var connection = new MySqlConnection(connectionString);
            var cmd = new MySqlCommand();
            cmd.Connection = connection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "Sp_Search_HashTags";
            cmd.Parameters.AddWithValue("_Location", sth.Location);
            connection.Open();
            var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                var TopHashCnt = new Serena_TopHashTags();

                TopHashCnt.Count = reader[0].ToString();
                TopHashCnt.Hashtags = reader[1].ToString();
                TopHashCnt.HashType = reader[2].ToString();
                lstAgeRange.Add(TopHashCnt);
            }
            connection.Close();
            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = JsonConvert.SerializeObject(lstAgeRange, Formatting.Indented),
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } },
            };

            return response;

        }

        public APIGatewayProxyResponse Get_PostsByHashTags_RDS(Serena_TopHashTags Sth)
        {
            var lstsuser = new List<Serena_Post>();

            var connection = new MySqlConnection(connectionString);
            var cmd = new MySqlCommand();
            cmd.Connection = connection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "Sp_PostsByHashTags";
            cmd.Parameters.AddWithValue("_UserEmailId", Sth.UserEmailId);
            cmd.Parameters.AddWithValue("_HashTags", Sth.Hashtags);
            connection.Open();
            var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                var sUser = new Serena_Post();
                sUser.Id = reader[0].ToString();
                sUser.UserEmailId = reader[1].ToString();
                sUser.Description = reader[2].ToString();
                sUser.ShareType = reader[3].ToString();
                sUser.Location = reader[4].ToString();
                sUser.Topics = reader[5].ToString();
                sUser.ImageUrl = reader[6].ToString();
                sUser.Tags = reader[7].ToString();
                sUser.HashTags = reader[8].ToString();
                sUser.LastCreated = reader[9].ToString();
                sUser.LastUpdated = reader[10].ToString();
                sUser.PostText = reader[11].ToString();
                sUser.PostColor = reader[12].ToString();
                sUser.BackgroundColor = reader[13].ToString();
                sUser.Opacity = Convert.ToDouble(reader[14]);
                sUser.PlaceAddress = reader[15].ToString();
                sUser.Longitude = reader[16].ToString();
                sUser.Latitude = reader[17].ToString();
                sUser.PostType = reader[18].ToString();
                sUser.Guid = reader[19].ToString();
                sUser.Favourite = reader[20].ToString();
                sUser.FavCount = reader[21].ToString();

                lstsuser.Add(sUser);
            }
            connection.Close();
            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = JsonConvert.SerializeObject(lstsuser, Formatting.Indented),
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } },
            };

            return response;
        }


        #endregion

        #region PostReviews
        public APIGatewayProxyResponse Get_PostReviews_RDS(Serena_PostReviews spr)
        {
            var lstAgeRange = new List<Serena_PostReviews>();

            var connection = new MySqlConnection(connectionString);
            var cmd = new MySqlCommand();
            cmd.Connection = connection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "Sp_Show_PostReviews";
            cmd.Parameters.AddWithValue("_Guid", spr.Guid);
            connection.Open();
            var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                var TopHashCnt = new Serena_PostReviews();

                TopHashCnt.UserEmailId = reader[0].ToString();
                TopHashCnt.FullName = reader[1].ToString();
                TopHashCnt.Review = reader[2].ToString();
                TopHashCnt.Rating = reader[3].ToString();
                TopHashCnt.DateAdded = reader[4].ToString();
                lstAgeRange.Add(TopHashCnt);
            }
            connection.Close();
            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = JsonConvert.SerializeObject(lstAgeRange, Formatting.Indented),
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } },
            };

            return response;

        }

        public APIGatewayProxyResponse Save_PostReview_RDS(Serena_PostReviews spr)
        {
            var connection = new MySqlConnection(connectionString);
            connection.Open();

            var cmd = new MySqlCommand();
            cmd.Connection = connection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "Sp_Insert_PostReviews";
            cmd.Parameters.AddWithValue("_Guid", spr.Guid);
            cmd.Parameters.AddWithValue("_UserEmailId", spr.UserEmailId);
            cmd.Parameters.AddWithValue("_Review", spr.Review);
            cmd.Parameters.AddWithValue("_Rating", spr.Rating);

            cmd.ExecuteNonQuery();
            connection.Close();

            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = "Record Saved Successfully!!!",
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } }
            };

            return response;

        }
        #endregion

        #region Top People
        public APIGatewayProxyResponse Get_TopPeople_RDS()
        {
            var lstAgeRange = new List<Serena_TopPeople>();

            var connection = new MySqlConnection(connectionString);
            var cmd = new MySqlCommand();
            cmd.Connection = connection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "Sp_Get_TOPPeople";
            connection.Open();
            var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                var ToppplCnt = new Serena_TopPeople();

                ToppplCnt.UserEmailId = reader[0].ToString();
                ToppplCnt.UserName = reader[1].ToString();
                ToppplCnt.UserProfileImage = reader[2].ToString();
                ToppplCnt.TotalReviews = reader[3].ToString();
                ToppplCnt.AvgRating = reader[4].ToString();
                ToppplCnt.Topics = reader[5].ToString();
                lstAgeRange.Add(ToppplCnt);
            }
            connection.Close();
            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = JsonConvert.SerializeObject(lstAgeRange, Formatting.Indented),
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } },
            };

            return response;

        }

        #endregion


        #region Pro members
        public APIGatewayProxyResponse Update_Serena_USerProfile_Pro(Serena_UserProfile_Pro upt)
        {

            var connection = new MySqlConnection(connectionString);
            connection.Open();
            var cmd = new MySqlCommand();
            cmd.Connection = connection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "Sp_Update_UserProfile_Pro";
            cmd.Parameters.AddWithValue("_UserEmailId", upt.UserEmailId);
            cmd.Parameters.AddWithValue("_FullName", upt.FullName);
            cmd.Parameters.AddWithValue("_UserName", upt.UserName);
            cmd.Parameters.AddWithValue("_UserWebsite", upt.UserWebsite);
            cmd.Parameters.AddWithValue("_UserLocation", upt.UserLocation);
            cmd.Parameters.AddWithValue("_AllowContact", upt.AllowContact);
            cmd.Parameters.AddWithValue("_ContactEmail", upt.ContactEmail);
            cmd.Parameters.AddWithValue("_ContactPhone", upt.ContactPhone);


            cmd.ExecuteNonQuery();
            connection.Close();
            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = "Record Updated Successfully!!!",
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } }
            };

            return response;
        }

        public APIGatewayProxyResponse Save_Serena_Pro_Social(Serena_Peo_Social upt)
        {

            var connection = new MySqlConnection(connectionString);
            connection.Open();
            var cmd = new MySqlCommand();
            cmd.Connection = connection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "Sp_Save_Serena_Pro_Social";
            cmd.Parameters.AddWithValue("_UserEmailId", upt.UserEmailId);
            cmd.Parameters.AddWithValue("_Categories", upt.Categories);
            cmd.Parameters.AddWithValue("_Tagline", upt.Tagline);
            cmd.Parameters.AddWithValue("_About", upt.About);
            cmd.Parameters.AddWithValue("_Coverphoto", upt.Coverphoto); 


            cmd.ExecuteNonQuery();
            connection.Close();
            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = "Record Updated Successfully!!!",
                Headers = new Dictionary<string, string> { { "ContentSp_GetNotifications-Type", "application/json" } }
            };

            return response;
        }

        public APIGatewayProxyResponse Get_Serena_Pro_Social(Serena_Peo_Social upt)
        {
            var lstAgeRange = new List<Serena_Peo_Social>();

            var connection = new MySqlConnection(connectionString);
            var cmd = new MySqlCommand();
            cmd.Connection = connection;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select * from Serena_Pro_Social where UserEmailId = '" + upt.UserEmailId + "'";
            connection.Open();
            var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                var ToppplCnt = new Serena_Peo_Social();

                ToppplCnt.UserEmailId = reader[0].ToString();
                ToppplCnt.Categories = reader[1].ToString();
                ToppplCnt.Tagline = reader[2].ToString();
                ToppplCnt.About = reader[3].ToString();
                ToppplCnt.Coverphoto = reader[4].ToString();
                lstAgeRange.Add(ToppplCnt);
            }
            connection.Close();
            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = JsonConvert.SerializeObject(lstAgeRange, Formatting.Indented),
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } },
            };

            return response;

        }

        public APIGatewayProxyResponse Update_Serena_Password(Serena_User_RDS su)
        {

            var connection = new MySqlConnection(connectionString);
            connection.Open();
            var cmd = new MySqlCommand();
            cmd.Connection = connection;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "update Serena_User set Password = '" + su.Password + "' where EmailId = '" + su.EMAIL + "'";
            cmd.ExecuteNonQuery();
            connection.Close();
            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = "Record Updated Successfully!!!",
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } }
            };

            return response;
        }

        public APIGatewayProxyResponse Get_Serena_Pro_Class(Serena_Pro_Class spc)
        {
            var lstAgeRange = new List<Serena_Pro_Class>();

            var connection = new MySqlConnection(connectionString);
            var cmd = new MySqlCommand();
            cmd.Connection = connection;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select * from Serena_Pro_Class where UserEmailId = '" + spc.UserEmailId + "'";
            connection.Open();
            var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                var ToppplCnt = new Serena_Pro_Class();

                ToppplCnt.Guid = reader[0].ToString();
                ToppplCnt.UserEmailId = reader[1].ToString();
                ToppplCnt.MediaUrl = reader[2].ToString();
                ToppplCnt.CoverPhotoUrl = reader[3].ToString();
                ToppplCnt.Title = reader[4].ToString();
                ToppplCnt.About = reader[5].ToString();
                ToppplCnt.PlayList = reader[6].ToString();
                ToppplCnt.Category = reader[7].ToString();
                ToppplCnt.Tags = reader[8].ToString();
                ToppplCnt.Mentions = reader[9].ToString();
                ToppplCnt.Duration = reader[10].ToString();
                ToppplCnt.DateAdded = reader[11].ToString();
                ToppplCnt.DateUpdated = reader[12].ToString();

                lstAgeRange.Add(ToppplCnt);
            }
            connection.Close();
            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = JsonConvert.SerializeObject(lstAgeRange, Formatting.Indented),
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } },
            };

            return response;

        }

        public APIGatewayProxyResponse Get_Serena_Pro_Class_ForAll()
        {
            var lstAgeRange = new List<Serena_Pro_Class>();

            var connection = new MySqlConnection(connectionString);
            var cmd = new MySqlCommand();
            cmd.Connection = connection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "Sp_Get_Serena_Pro_Class_ForAll";
            connection.Open();
            var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                var ToppplCnt = new Serena_Pro_Class();

                ToppplCnt.Guid = reader[0].ToString();
                ToppplCnt.UserEmailId = reader[1].ToString();
                ToppplCnt.MediaUrl = reader[2].ToString();
                ToppplCnt.CoverPhotoUrl = reader[3].ToString();
                ToppplCnt.Title = reader[4].ToString();
                ToppplCnt.About = reader[5].ToString();
                ToppplCnt.PlayList = reader[6].ToString();
                ToppplCnt.Category = reader[7].ToString();
                ToppplCnt.Tags = reader[8].ToString();
                ToppplCnt.Mentions = reader[9].ToString();
                ToppplCnt.Duration = reader[10].ToString();
                ToppplCnt.DateAdded = reader[11].ToString();
                ToppplCnt.DateUpdated = reader[12].ToString();
                ToppplCnt.AvgRating = reader[13].ToString();
                ToppplCnt.TotalReviews = reader[14].ToString();

                lstAgeRange.Add(ToppplCnt);
            }
            connection.Close();
            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = JsonConvert.SerializeObject(lstAgeRange, Formatting.Indented),
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } },
            };

            return response;

        }
        public APIGatewayProxyResponse Save_Serena_Pro_Class(Serena_Pro_Class spc)
        {

            var connection = new MySqlConnection(connectionString);
            connection.Open();
            var cmd = new MySqlCommand();
            cmd.Connection = connection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "Sp_Save_Serena_Pro_Class";
            cmd.Parameters.AddWithValue("_Guid", spc.Guid);
            cmd.Parameters.AddWithValue("_UserEmailId", spc.UserEmailId);
            cmd.Parameters.AddWithValue("_MediaUrl", spc.MediaUrl);
            cmd.Parameters.AddWithValue("_CoverPhotoUrl", spc.CoverPhotoUrl);
            cmd.Parameters.AddWithValue("_Title", spc.Title);
            cmd.Parameters.AddWithValue("_About", spc.About);
            cmd.Parameters.AddWithValue("_PlayList", spc.PlayList);
            cmd.Parameters.AddWithValue("_Category", spc.Category);
            cmd.Parameters.AddWithValue("_Tags", spc.Tags);
            cmd.Parameters.AddWithValue("_Mentions", spc.Mentions);
            cmd.Parameters.AddWithValue("_Duration", spc.Duration);
            cmd.ExecuteNonQuery();
            connection.Close();
            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = "Record Updated Successfully!!!",
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } }
            };

            return response;
        }

        public APIGatewayProxyResponse Get_Serena_Notifications(Serena_Notifications SN)
        {
            var lstAgeRange = new List<Serena_Notifications>();

            var connection = new MySqlConnection(connectionString);
            var cmd = new MySqlCommand();
            cmd.Connection = connection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "Sp_GetNotifications";
            cmd.Parameters.AddWithValue("_UserEmailId", SN.UserEmailId);
            connection.Open();
            var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                var ToppplCnt = new Serena_Notifications();

                ToppplCnt.CountORRequestBy = reader[0].ToString();
                ToppplCnt.Guid = reader[1].ToString();
                ToppplCnt.LastUpdated = reader[2].ToString();
                ToppplCnt.ImageUrl = reader[3].ToString();
                ToppplCnt.PostText = reader[4].ToString();
                ToppplCnt.PostColor = reader[5].ToString();
                ToppplCnt.BackgroundColor = reader[6].ToString();
                ToppplCnt.PostType = reader[7].ToString();
                ToppplCnt.category = reader[8].ToString(); 
                lstAgeRange.Add(ToppplCnt);
            }
            connection.Close();
            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = JsonConvert.SerializeObject(lstAgeRange, Formatting.Indented),
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } },
            };

            return response;

        }

        public APIGatewayProxyResponse SearchClass_Explore()
        {
            var lstAgeRange = new List<Serena_SearchClassExplore>();

            var connection = new MySqlConnection(connectionString);
            var cmd = new MySqlCommand();
            cmd.Connection = connection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "Sp_SearchClass_Explore";
            connection.Open();
            var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                var ToppplCnt = new Serena_SearchClassExplore();

                ToppplCnt.AvgRating = reader[0].ToString();
                ToppplCnt.TotalReviews = reader[1].ToString();
                ToppplCnt.guid = reader[2].ToString();
                ToppplCnt.Title = reader[3].ToString();
                ToppplCnt.CoverPhotoUrl = reader[4].ToString();
                ToppplCnt.Mentions = reader[5].ToString();

                lstAgeRange.Add(ToppplCnt);
            }
            connection.Close();
            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = JsonConvert.SerializeObject(lstAgeRange, Formatting.Indented),
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } },
            };

            return response;

        }
        #endregion

        #region watchParty
        public APIGatewayProxyResponse Save_Serena_watchparty(Serena_watchParty upt)
        {

            var connection = new MySqlConnection(connectionString);
            connection.Open();
            var cmd = new MySqlCommand();
            cmd.Connection = connection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "Sp_Insert_Serena_watchparty";
            cmd.Parameters.AddWithValue("_Guid", upt.Guid);
            cmd.Parameters.AddWithValue("_UserEmailId", upt.UserEmailId);
            cmd.Parameters.AddWithValue("_ClassGuid", upt.ClassGuid);
            cmd.Parameters.AddWithValue("_ScheduleDate", upt.ScheduleDate);
            cmd.Parameters.AddWithValue("_ScheduleTime", upt.ScheduleTime);
            cmd.Parameters.AddWithValue("_AMPM", upt.AMPM);
            cmd.Parameters.AddWithValue("_MarkToCalendar", upt.MarkToCalendar);
            cmd.Parameters.AddWithValue("_RemindViaApp", upt.RemindViaApp);
            cmd.Parameters.AddWithValue("_RemindViaTxt", upt.RemindViaTxt); 

            cmd.ExecuteNonQuery();
            connection.Close();
            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = "Record Updated Successfully!!!",
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } }
            };

            return response;
        }

        public APIGatewayProxyResponse Insert_Serena_watchparty_Members(Serena_watchParty upt)
        {

            var connection = new MySqlConnection(connectionString);
            connection.Open();
            var cmd = new MySqlCommand();
            cmd.Connection = connection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "Sp_Insert_Serena_watchparty_Members";
            cmd.Parameters.AddWithValue("_WatchPartyGuid", upt.Guid);
            cmd.Parameters.AddWithValue("_UserEmailId", upt.Members);

            cmd.ExecuteNonQuery();
            connection.Close();
            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = "Record Updated Successfully!!!",
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } }
            };

            return response;
        }

        public APIGatewayProxyResponse Get_watchPartyDetails(Serena_watchParty swp)
        {
            var lstAgeRange = new List<Serena_watchParty>();

            var connection = new MySqlConnection(connectionString);
            var cmd = new MySqlCommand();
            cmd.Connection = connection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "Sp_Get_watchPartyDetails";
            cmd.Parameters.AddWithValue("_Guid", swp.Guid);

            connection.Open();
            var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                var ToppplCnt = new Serena_watchParty();

                ToppplCnt.Guid = reader[0].ToString();
                ToppplCnt.UserEmailId = reader[1].ToString();
                ToppplCnt.ClassGuid = reader[2].ToString();
                ToppplCnt.ScheduleDate = reader[3].ToString();
                ToppplCnt.ScheduleTime = reader[4].ToString();
                ToppplCnt.AMPM = reader[5].ToString();
                ToppplCnt.MarkToCalendar = reader[6].ToString();
                ToppplCnt.RemindViaApp = reader[7].ToString();
                ToppplCnt.RemindViaTxt = reader[8].ToString();
                ToppplCnt.Members = reader[9].ToString(); 

                lstAgeRange.Add(ToppplCnt);
            }
            connection.Close();
            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = JsonConvert.SerializeObject(lstAgeRange, Formatting.Indented),
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } },
            };

            return response;

        }

        public APIGatewayProxyResponse Get_UpcommingActivities(Serena_upcommingEventsNot sue)
        {
            var lstAgeRange = new List<Serena_upcommingEventsNot>();

            var connection = new MySqlConnection(connectionString);
            var cmd = new MySqlCommand();
            cmd.Connection = connection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "Sp_Get_UpcommingActivities";
            cmd.Parameters.AddWithValue("_UserEmailId", sue.UserEmailId);
            connection.Open();
            var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                var ToppplCnt = new Serena_upcommingEventsNot();

                ToppplCnt.ClassGuid = reader[0].ToString();
                ToppplCnt.MediaUrl = reader[1].ToString();
                ToppplCnt.CoverPhotoUrl  = reader[2].ToString();
                ToppplCnt.Title  = reader[3].ToString();
                ToppplCnt.About  = reader[4].ToString();
                ToppplCnt.Category  = reader[5].ToString();
                ToppplCnt.Tags  = reader[6].ToString();
                ToppplCnt.Mentions  = reader[7].ToString();
                ToppplCnt.Duration  = reader[8].ToString();
                ToppplCnt.WatchPartyGuid  = reader[9].ToString();
                ToppplCnt.ScheduledDate  = reader[10].ToString();
                ToppplCnt.ScheduledTime  = reader[11].ToString();
                ToppplCnt.AMPM  = reader[12].ToString();
                ToppplCnt.HostEmailID  = reader[13].ToString();
                ToppplCnt.Accepted  = reader[14].ToString();
                ToppplCnt.AvgRating = reader[15].ToString();
                ToppplCnt.TotalReviews = reader[16].ToString();
                ToppplCnt.Members = reader[17].ToString();

                lstAgeRange.Add(ToppplCnt);
            }
            connection.Close();
            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = JsonConvert.SerializeObject(lstAgeRange, Formatting.Indented),
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } },
            };

            return response;

        }


        public APIGatewayProxyResponse Delete_Serena_watchparty(Serena_watchParty upt)
        {

            var connection = new MySqlConnection(connectionString);
            connection.Open();
            var cmd = new MySqlCommand();
            cmd.Connection = connection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "Sp_delete_Watchparty";
            cmd.Parameters.AddWithValue("_Guid", upt.Guid);

            cmd.ExecuteNonQuery();
            connection.Close();
            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = "Record Updated Successfully!!!",
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } }
            };

            return response;
        }

        public APIGatewayProxyResponse Update_Serena_watchparty(Serena_watchParty upt)
        {

            var connection = new MySqlConnection(connectionString);
            connection.Open();
            var cmd = new MySqlCommand();
            cmd.Connection = connection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "Sp_update_Watchparty";
            cmd.Parameters.AddWithValue("_Guid", upt.Guid);
            cmd.Parameters.AddWithValue("_ScheduleDate", upt.ScheduleDate);
            cmd.Parameters.AddWithValue("_ScheduleTime", upt.ScheduleTime);



            cmd.ExecuteNonQuery();
            connection.Close();
            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = "Record Updated Successfully!!!",
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } }
            };

            return response;
        }


        public APIGatewayProxyResponse Accept_Serena_watchparty(Serena_watchParty upt)
        {

            var connection = new MySqlConnection(connectionString);
            connection.Open();
            var cmd = new MySqlCommand();
            cmd.Connection = connection;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "update Serena_watchparty_Members set Accepted = '" + upt.Accepted + "' where WatchPartyGuid = '" + upt.Guid + "' and UserEmailId = '" + upt.Members + "'";
            
            cmd.ExecuteNonQuery();
            connection.Close();
            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = "Record Updated Successfully!!!",
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } }
            };

            return response;
        }
        #endregion

        #region  watch party

        public APIGatewayProxyResponse Insert_Serena_bookmark(Serena_Bookmark upt)
        {

            var connection = new MySqlConnection(connectionString);
            connection.Open();
            var cmd = new MySqlCommand();
            cmd.Connection = connection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "Sp_insertinto_Serena_bookmark";
            cmd.Parameters.AddWithValue("_UserEmailId", upt.UserEmailId);
            cmd.Parameters.AddWithValue("_Guid", upt.Guid);
            cmd.Parameters.AddWithValue("_category", upt.category);
            cmd.Parameters.AddWithValue("_comments", upt.comments);

            cmd.ExecuteNonQuery();
            connection.Close();
            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = "Record Updated Successfully!!!",
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } }
            };

            return response;
        }

        public APIGatewayProxyResponse Get_Userbookmarks(Serena_Bookmark upt)
        {
            var lstbkm = new List<Serena_Bookmark>();

            var connection = new MySqlConnection(connectionString);
            var cmd = new MySqlCommand();
            cmd.Connection = connection;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select * from Serena_bookmark where UserEmailId = '" + upt.UserEmailId + "' ";
             
            connection.Open();
            var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                var bokmrk = new Serena_Bookmark();

                bokmrk.UserEmailId = reader[0].ToString();
                bokmrk.Guid = reader[1].ToString();
                bokmrk.category = reader[2].ToString();
                bokmrk.comments = reader[3].ToString();
                bokmrk.datecreated = reader[4].ToString(); 

                lstbkm.Add(bokmrk);
            }
            connection.Close();
            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = JsonConvert.SerializeObject(lstbkm, Formatting.Indented),
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } },
            };

            return response;

        }
        #endregion

    }
}
