﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SERENA_AWS_SERVICES
{
    class Serena_TopHashTags2
    {
        public string Count { get; set; }
        public string Hashtags { get; set; }
        public string  ImageUrl1 { get; set; }
        public string  PostText1  { get; set; }
        public string  PostColor1  { get; set; }
        public string  BackgroundColor1  { get; set; } 
        public string  PlaceAddress1  { get; set; }
        public string  Longitude1  { get; set; }
        public string  Latitude1  { get; set; }
        public string  PostType1  { get; set; }
        public string  ImageUrl2  { get; set; }
        public string  PostText2  { get; set; }
        public string  PostColor2  { get; set; }
        public string  BackgroundColor2  { get; set; } 
        public string  PlaceAddress2  { get; set; }
        public string  Longitude2  { get; set; }
        public string  Latitude2  { get; set; }
        public string  PostType2  { get; set; }
        public string  ImageUrl3  { get; set; }
        public string  PostText3  { get; set; }
        public string  PostColor3  { get; set; }
        public string  BackgroundColor3  { get; set; }
        public string  PlaceAddress3  { get; set; }
        public string  Longitude3  { get; set; }
        public string  Latitude3  { get; set; }
        public string  PostType3  { get; set; }
        public string  ImageUrl4  { get; set; }
        public string  PostText4  { get; set; }
        public string  PostColor4  { get; set; }
        public string  BackgroundColor4  { get; set; }
        public string  PlaceAddress4  { get; set; }
        public string  Longitude4  { get; set; }
        public string  Latitude4  { get; set; }
        public string  PostType4  { get; set; }
        public string  ImageUrl5  { get; set; }
        public string  PostText5  { get; set; }
        public string  PostColor5  { get; set; }
        public string  BackgroundColor5  { get; set; }
        public string  PlaceAddress5  { get; set; }
        public string  Longitude5  { get; set; }
        public string  Latitude5  { get; set; }
        public string  PostType5  { get; set; }
    }
}
