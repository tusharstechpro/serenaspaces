﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SERENA_AWS_SERVICES
{
    public class Serena_upcommingEventsNot
    {
        public string UserEmailId { get; set; }

        public string ClassGuid { get; set; }
        public string MediaUrl { get; set; }
        public string CoverPhotoUrl { get; set; }
        public string Title { get; set; }
        public string About { get; set; }
        public string Category { get; set; }
        public string Tags { get; set; }
        public string Mentions { get; set; }
        public string Duration { get; set; }
        public string WatchPartyGuid { get; set; }
        public string ScheduledDate { get; set; }
        public string ScheduledTime { get; set; }
        public string AMPM { get; set; }
        public string HostEmailID { get; set; }
        public string Accepted { get; set; }
        public string AvgRating { get; set; }
        public string TotalReviews { get; set; }
        public string Members { get; set; }

    }
}
