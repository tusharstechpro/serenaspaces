﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SERENA_AWS_SERVICES
{
    public class Serena_watchParty
    {
        public string Guid { get; set; }
        public string UserEmailId { get; set; }
        public string ClassGuid { get; set; }
        public string ScheduleDate { get; set; }
        public string ScheduleTime { get; set; }
        public string AMPM { get; set; }
        public string MarkToCalendar { get; set; }
        public string RemindViaApp { get; set; }
        public string RemindViaTxt { get; set; }
        public string Members { get; set; }
        public string Accepted { get; set; }


    }
}
