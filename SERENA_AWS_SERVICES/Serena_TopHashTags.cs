﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SERENA_AWS_SERVICES
{
    public class Serena_TopHashTags
    {
        public string Hashtags { get; set; }
        public string Count { get; set; }
        public string Img1 { get; set; }
        public string Img2 { get; set; }
        public string Img3 { get; set; }
        public string HashType { get; set; }
        public string Location { get; set; }
        public string UserEmailId { get; set; }
    }
}
