﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SERENA_AWS_SERVICES
{
    public class Serena_PostReviews
    {
        public string Guid { get; set; }
        public string UserEmailId { get; set; }
        public string FullName { get; set; }
        public string Review { get; set; }
        public string DateAdded { get; set; }
        public string Rating { get; set; }

    }
}
