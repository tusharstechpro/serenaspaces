﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SERENA_AWS_SERVICES
{
    public class Serena_Peo_Social
    {
        public string UserEmailId { get; set; }
        public string Categories { get; set; }
        public string Tagline { get; set; }
        public string About { get; set; }
        public string Coverphoto { get; set; }
    }
}
