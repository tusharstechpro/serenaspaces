﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace SERENA_AWS_SERVICES
{
    public class Serena_User
    {
        //Email ID
        public string Id { get; set; }
        public string Password { get; set; }
        public string FullName { get; set; }
        public string UserName { get; set; }
        public string Place { get; set; }
        public string UserCategory { get; set; }
        public string UserLoginType { get; set; }
        public string UserStatus { get; set; }
        public string LastCreated { get; set; }
        public string LastUpdated { get; set; }
        public string LastLoginTime { get; set; }
         
    }
}
