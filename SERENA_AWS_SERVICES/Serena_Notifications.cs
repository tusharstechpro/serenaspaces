﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SERENA_AWS_SERVICES
{
    public class Serena_Notifications
    {
        public string CountORRequestBy { get; set; }
        public string Guid { get; set; }
        public string ImageUrl { get; set; }
        public string PostText { get; set; }
        public string PostColor { get; set; }
        public string BackgroundColor { get; set; }
        public string PostType { get; set; }
        public string category { get; set; } 
        public string UserEmailId { get; set; }
        public string LastUpdated { get; set; }

    }
}
