﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SERENA_AWS_SERVICES
{
    public class Serena_UserProfile_RDS
    {
        public string Id { get; set; }
        public string UserEmailId { get; set; }
        public string Guid { get; set; }
        public string UserPostJson { get; set; }
        public string UserProfileImage { get; set; }
        public string UserAccountType { get; set; }
        public string UserBio { get; set; }
        public string UserWebsite { get; set; }
        public string UserLocation { get; set; }
        public string UserAgeRange { get; set; }
        public string UserName { get; set; }
        public string TrequestStatus { get; set; }
        public string UserPost { get; set; }
        public string LastUpdated { get; set; }
        public string AllowContact { get; set; }
        public string ContactEmail { get; set; }
        public string ContactPhone { get; set; }
        public string IsPro { get; set; }
    }
}
