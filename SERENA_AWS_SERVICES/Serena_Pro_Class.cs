﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SERENA_AWS_SERVICES
{
    public class Serena_Pro_Class
    {
        public string Guid { get; set; }
        public string UserEmailId { get; set; }
        public string MediaUrl { get; set; }
        public string CoverPhotoUrl { get; set; }
        public string Title { get; set; }
        public string About { get; set; }
        public string PlayList { get; set; }
        public string Category { get; set; }
        public string Tags { get; set; }
        public string Mentions { get; set; }
        public string Duration { get; set; }
        public string DateAdded { get; set; }
        public string DateUpdated { get; set; }
        public string AvgRating { get; set; }
        public string TotalReviews { get; set; }


    }
}
