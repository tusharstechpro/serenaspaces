﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SERENA_AWS_SERVICES
{
    public class Serena_UprofileTran_RDS
    {
        public string Id { get; set; }
        public string UserEmailId { get; set; }
        public string RequestTo { get; set; }
        public string Status { get; set; }
        public string Guid { get; set; }

    }
}
