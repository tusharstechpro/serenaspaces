﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SERENA_AWS_SERVICES
{
    public class Serena_UserProfile
    {
        public string Id { get; set; }
        public string UserEmailId { get; set; }
        public string UserFollowerJson { get; set; }
        public string UserFollowingJson { get; set; }
        public string UserPostJson { get; set; }
        public string UserProfileImage { get; set; }
        public string UserAccountType { get; set; }
        public string UserBio { get; set; }
        public string UserWebsite { get; set; }
        public string UserLocation { get; set; }
        public string UserAgeRange { get; set; }

    }
}
